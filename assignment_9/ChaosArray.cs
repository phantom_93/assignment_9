﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_9
{
    class ChaosArray
    {
        #region Variables
        private object[] oArray; // array that stores all the data 
        private Random random; // Random generator 
        #endregion


        #region Properties
        public int Length { get; private set; } // length of the collection
        #endregion


        #region Constructors
        // constructor with initialization 
        public ChaosArray(int length = 20)
        {
            //here the variables will initialize 
            random = new Random(); 
            Length = length; 
            oArray = new object[length];
           

        }
        #endregion


        #region Functions
        /* Functions 
         * 1. Insert 
           2. Retrieve
           3. Remove
           4. check */
        //the following function insert one object at a time.
        public void Insert(params object[] array)
        {
            foreach (object obj in array)
            {
                int index = GetAvailableIndex();
                //Console.WriteLine($"Available index: {index} = {obj.ToString()}");
                oArray[index] = obj;
            }
        }

        // retrieves the given object one at a time 
        public object Retrieve()
        {
            int index = GetAllocatedIndex();

            return oArray[index];
        }

        // remove the given object one at a time 
        public object Remove()
        {
            int index = GetAllocatedIndex();
            object val = oArray[index];
            oArray[index] = default(object);

            return val;
        }

        // the following function checks avaliable index 
        private int GetAvailableIndex()
        {
            List<int> indexes = new List<int>();

            for (int i = 0; i < oArray.Length; i++)
            {
                // if there are empty indexes, add objects to it 
                if (IsIndexEmpty(i))
                {
                    indexes.Add(i);
                }
            }
            // if indexes are not empty, throw exceptions 
            if (indexes.Count == 0)
            {
                throw new FullArrayException("The chaos array is full.");
            }

            // return empty random indexes 
            return indexes[random.Next(0, indexes.Count)];
        }

        //the following function gets the number of indexes that are full 
        private int GetAllocatedIndex()
        {

            List<int> indexes = new List<int>();

            for (int i = 0; i < oArray.Length; i++)
            {
                // if the indexes are taken, add the index to the list of allocated indexes 
                if (!IsIndexEmpty(i))
                {
                    indexes.Add(i);
                }
            }

            if (indexes.Count == 0)
            {
                throw new EmptyArrayException("The choas array is empty.");
            }

            return indexes[random.Next(0, indexes.Count)];
        }

        private bool IsIndexEmpty(int index)
        {
            // return true if the given index is Empty else false
            return oArray[index] == default(object);
        }
        #endregion


    }
}
