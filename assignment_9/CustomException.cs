﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_9
{
    #region EmptyArrayException
    // when retrive or remove while chaosArray is Empty
    public class EmptyArrayException : Exception
    {
        public EmptyArrayException()
        {
        }

        public EmptyArrayException(string message)
            : base(message)
        {
        }

        public EmptyArrayException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
    #endregion
    // when retrive or remove while chaosArray is Full

    #region FullArrayException
    public class FullArrayException : Exception
    {
        public FullArrayException()
        {
        }

        public FullArrayException(string message)
            : base(message)
        {
        }

        public FullArrayException(string message, Exception inner)
            : base(message, inner)
        {
        }
        #endregion
    }
}
