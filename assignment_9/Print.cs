﻿using System;
using System.Collections.Generic;
using System.Text;

namespace assignment_9
{
    
    public class Print
    {
        #region PrintDescription Method
        public static void PrintDescription()
        {
           
            PrintLine("This program will store the items you entered in random order");
            PrintLine("\nHere are your options:", ConsoleColor.DarkYellow);
            PrintLine(" - insert item (For instance: add beer, chocolate)", ConsoleColor.Green);
            PrintLine(" - remove (will delete any item from the program randomly)", ConsoleColor.Red);
            PrintLine(" - retrieve (will get any item from program randomly)", ConsoleColor.DarkYellow);
            PrintLine(" - exit (Quit the application)", ConsoleColor.DarkRed);
            PrintLine("\n");
        }
        #endregion

        #region  PrintLine Method 
        public static void PrintLine(string line, ConsoleColor color = ConsoleColor.White)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(line);
            Console.ForegroundColor = ConsoleColor.White;
        }
        #endregion
    }
}
