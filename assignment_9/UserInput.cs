﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace assignment_9
{
    class UserInput
    {
        static ChaosArray chaosArray = new ChaosArray(10);
        #region UserInputCommands Method
        public static void UserInputCommands()
        {
            bool operation = true;

            while (operation)
            {
                Console.Write("> ");
                string userInput = Console.ReadLine();

                try
                {
                    #region Switch statements
                    switch (userInput)
                    {
                        case "retrieve":
                            object objRetrieved = chaosArray.Retrieve();
                            Print.PrintLine($"Retrieved '{objRetrieved.ToString()}'", ConsoleColor.DarkYellow);
                            break;
                        case "remove":
                            object objRemoved = chaosArray.Remove();
                            Print.PrintLine($"Removed '{objRemoved.ToString()}'", ConsoleColor.Red);
                            break;
                        case "exit":
                            operation = false;
                            break;
                        default:
                            string[] contents = userInput.Split(" "); // remove spaces 
                            if (contents[0].Equals("insert"))
                            {
                                string item = userInput.Remove(0, 7); // remove the first 4 words

                             
                                if (int.TryParse(item, out int userInt))
                                {
                                    
                                    chaosArray.Insert(userInt);
                                }
                                else if (double.TryParse(item, out double userDouble))
                                {
                                    
                                    chaosArray.Insert(userDouble);
                                }
                                else if (char.TryParse(item, out char userChar))
                                {
                                    
                                    chaosArray.Insert(userChar);
                                }
                                else if (Boolean.TryParse(item, out bool userBoolean))
                                {
                                    
                                    chaosArray.Insert(userBoolean);
                                }
                                else
                                    chaosArray.Insert(item);

                                Print.PrintLine($"Added '{item}' to the array.", ConsoleColor.Green);

                                if (contents.Length <= 1)
                                {
                                    throw new Exception("You haven't specified anything to add.");
                                }

                            }

                            break;
                            #endregion
                    }
                }
                catch (Exception ex)
                {
                    Print.PrintLine($"Error: {ex.Message}", ConsoleColor.Red);
                }
            }
        }
        #endregion
    }
}
